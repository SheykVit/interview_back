import {Module} from '@nestjs/common';
import {UsersService} from './users.service';
import {UsersController} from './users.controller';
import {DbSequenceService} from '../services/db-sequence.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CounterSchema} from '../schemas/counter.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Counter', schema: CounterSchema }]),
    ],
    providers: [UsersService, DbSequenceService],
    exports: [UsersService],
    controllers: [UsersController],
})
export class UsersModule {
}
