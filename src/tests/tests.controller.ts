import {Body, Controller, Get, Param, Post, Request} from '@nestjs/common';
import {DbSequenceService} from '../services/db-sequence.service';
import {TestsService} from './tests.service';
import {Observable} from 'rxjs';
import {CreateTestDto} from './create-test.dto';

@Controller('tests')
export class TestsController {
    constructor(private service: TestsService) {
    }

    @Get(':id')
    findOne(@Param('id') id) {
        return this.service.findOne(id);
    }
    /**
     * СОздание нового теста
     * @param createTestDto
     */
    @Post('store')
    store(@Body() createTestDto: CreateTestDto) {
        return this.service.store(createTestDto);
    }

    /**
     * Обновление теста
     * @param id
     * @param createTestDto
     */
    @Post(':id/update')
    update(
        @Param('id') id,
        @Body() createTestDto: CreateTestDto
        ) {
        return this.service.update(id, createTestDto);
    }
}
