import {Injectable} from '@nestjs/common';
import {DbSequenceService} from '../services/db-sequence.service';
import {InjectModel} from '@nestjs/mongoose';
import {CreateTestDto} from './create-test.dto';

@Injectable()
export class TestsService {
    constructor(
        private seqService: DbSequenceService,
        @InjectModel('Test') private testModel
    ) {
    }

    /**
     * Создание ноовго теста
     */
    async store(data: CreateTestDto): Promise<any> {
        const id = await this.seqService.getNextSequenceValue('testId');
        const prepareData = {
            _id: id,
            ...data
        };
        const createTest = new this.testModel(prepareData);
        return createTest.save();
    }

    /**
     * Обновление теста
     */
    async update(id: number, data: CreateTestDto): Promise<any> {
        let doc = await this.testModel.findOneAndUpdate(
            {_id: id},
            {...data},
            {new: true},
        );
        return doc;
    }

    async findOne(id: number) {
        let doc = await this.testModel.findOne(
            {_id: id}
        );
        return doc;
    }
}
