import {Module} from '@nestjs/common';
import {TestsController} from './tests.controller';
import {DbSequenceService} from '../services/db-sequence.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CounterSchema} from '../schemas/counter.schema';
import {TestsService} from './tests.service';
import {TestSchema} from '../schemas/test.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Counter', schema: CounterSchema },
            { name: 'Test', schema: TestSchema },
            ]),
    ],
    providers: [DbSequenceService, TestsService],
    controllers: [TestsController],
})
export class TestsModule {
}
