import {IsArray, IsInt, IsOptional, IsString, ValidateNested} from 'class-validator';
import {QuestionDto} from './question.dto';
import {Type} from 'class-transformer';

export class CreateTestDto {

    @IsOptional()
    id?: number;

    @IsString()
    categoryId: number;

    @IsString()
    title: string;

    @IsOptional()
    @IsArray()
    @ValidateNested({each: true})
    @Type(() => QuestionDto)
    questions?: QuestionDto[];

}
