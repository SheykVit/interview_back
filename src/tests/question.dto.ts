import {IsNotEmpty, IsString} from 'class-validator';

export class QuestionDto {

    @IsNotEmpty()
    @IsString()
    text: string;

    @IsNotEmpty()
    @IsString()
    answer: string;
}
