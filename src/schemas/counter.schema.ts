import * as mongoose from 'mongoose';

export const CounterSchema = new mongoose.Schema({
    _id: String,
    sequence_value: Number
});
