import * as mongoose from 'mongoose';

export const QuestionSchema = new mongoose.Schema({
    text: String,
    answer: String
});
