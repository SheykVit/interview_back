import * as mongoose from 'mongoose';
import {QuestionSchema} from './question.schema';
import * as moment from 'moment';

export const TestSchema = new mongoose.Schema({
    _id: Number,
    categoryId: Number,
    title: String,
    createdAt: {type: String, default: moment().local().format('DD.MM.YYYY HH:mm:ss')},
    updatedAt: {type: String, default: moment().local().format('DD.MM.YYYY HH:mm:ss')},
    questions: [QuestionSchema]
});
