export const CONFIG_OPTIONS = Symbol();
export type Mode = 'dev' | 'prod';
export const defaultConfigOptions = {
    folder: './config',
};
