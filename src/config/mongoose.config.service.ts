import {Injectable} from '@nestjs/common';
import {MongooseModuleOptions, MongooseOptionsFactory} from '@nestjs/mongoose';
import {ConfigService} from './config.service';

@Injectable()
export default class MongooseConfigService implements MongooseOptionsFactory {
    constructor(private config: ConfigService) {}

    createMongooseOptions(): MongooseModuleOptions {
        return {
            uri: `mongodb://${this.config.get('DATABASE_USER')}:${this.config.get('DATABASE_PASS')}@${this.config.get('DATABASE_HOST')}:${this.config.get('DATABASE_PORT')}/${this.config.get('DATABASE_DB')}?authSource=admin&w=1`,
        };
    }
}
