import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {defaultConfigOptions} from './config/constants';
import {ConfigModule} from './config/config.module';
import {MongooseModule} from '@nestjs/mongoose';
import MongooseConfigService from './config/mongoose.config.service';
import {UsersModule} from './users/users.module';
import {AuthModule} from './auth/auth.module';
import {DbSequenceService} from './services/db-sequence.service';
import {CounterSchema} from './schemas/counter.schema';
import {TestsModule} from './tests/tests.module';

@Module({
    imports: [
        ConfigModule.register(defaultConfigOptions),
        MongooseModule.forRootAsync({
            useClass: MongooseConfigService,
        }),
        MongooseModule.forFeature([{name: 'Counter', schema: CounterSchema}]),
        UsersModule,
        AuthModule,
        TestsModule
    ],
    controllers: [AppController],
    providers: [AppService, DbSequenceService],
})
export class AppModule {
}
