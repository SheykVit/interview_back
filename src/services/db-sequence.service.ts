import {Injectable} from '@nestjs/common';
import {Connection} from 'mongoose';
import {InjectConnection, InjectModel} from '@nestjs/mongoose';

@Injectable()
export class DbSequenceService {
    constructor(
        @InjectConnection() private db: Connection,
        @InjectModel('Counter') private counterModel) {
    }

    public async getNextSequenceValue(sequenceName) {
        let doc = await this.counterModel.findOneAndUpdate(
            {_id: sequenceName},
            {$inc: {sequence_value: 1}},
            {new: true},
        );
        return doc.sequence_value;
    }
}
