import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {LocalStrategy} from './local.strategy';
import {UsersModule} from '../users/users.module';
import {PassportModule} from '@nestjs/passport';
import {JwtModule} from '@nestjs/jwt';
import {JwtStrategy} from './jwt.strategy';
import {ConfigService} from '../config/config.service';
import {ConfigModule} from '../config/config.module';
import {defaultConfigOptions} from '../config/constants';
import {MongooseModule} from '@nestjs/mongoose';
import {UserSchema} from '../schemas/user.schema';

@Module({
    imports: [
        UsersModule,
        PassportModule,
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                    secret: configService.get('JWT_SECRET_KEY'),
                    signOptions: {expiresIn: configService.get('JWT_EXPIRES')},
                }
            ),
            inject: [ConfigService],
        }),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
})
export class AuthModule {
}
