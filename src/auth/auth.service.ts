import {Injectable, ParseIntPipe, UnauthorizedException, UsePipes, ValidationPipe} from '@nestjs/common';
import {UsersService} from '../users/users.service';
import {JwtService} from '@nestjs/jwt';
import {InjectModel} from '@nestjs/mongoose';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        @InjectModel('User') private userModel,
    ) {
    }

    async validateUser({username, password}): Promise<any> {
        let query = this.userModel.where({
            username: username
        });
        const user = await query.findOne();
        if (user) {
            const bcrypt = require('bcrypt');
            const result = await new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (!result) reject(err);
                    resolve(1);
                });
            });
            if (!result) {
                throw Error('Wrong password');
            }
            return user;
        } else {
            throw Error('User not found');
        }
    }

    async login(user: { username: string, password: string }) {
        const payload = {username: user.username, password: user.password};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
